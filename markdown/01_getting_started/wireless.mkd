Wireless
--------

To know if your card is supported the best things to do is to check this
website:

http://linuxwireless.org/

You will find all the links to the different drivers that your card needs, and
the optionnal firmware. Please note, that everything named sta or staging
usually means that it is a software that does not have the quality required yet
to be merged in the main Linux tree, but are provided so that at least you can
try out things right away. Depending on your hardware you could be lucky or
unlucky with a staging driver.

If Puredyne is missing software/firmware/modules for your card, please check
this website first so that you can point us to the right info and we can see
what is possible.
